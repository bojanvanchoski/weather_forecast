<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WeatherForecast extends Model
{
    protected $fillable = [
        'time',
        'temperature',
        'humidity',
        'rain',
        'wind',
        'battery_level',
        'light',
        'station_id'
    ];
}
