<?php

namespace App\Services;

interface WeatherDataParser
{
    public function parse(string $fileName);
}
