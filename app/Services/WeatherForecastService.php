<?php

namespace App\Services;

use App\Repositories\WeatherForecastInterface;
use App\WeatherForecast;
use App\WeatherStation;
use Carbon\Carbon;

class WeatherForecastService
{
    protected $weatherForecastInterface;

    public function __construct(WeatherForecastInterface $weatherForecastInterface)
    {
        $this->weatherForecastInterface = $weatherForecastInterface;
    }

    public function getWeatherForecastByStation(WeatherStation $weatherStation, Carbon $startDate)
    {
        $startDate->setMinutes(0)->setSeconds(0);
        $endDate = clone($startDate);
        $endDate->setMinutes(59)->setSeconds(59);

        return $this->weatherForecastInterface->getWeatherForecastByStation($weatherStation, $startDate, $endDate);
    }

    public function getAverageWeatherForecastByDate(string $date)
    {
        return $this->weatherForecastInterface->getAverageWeatherForecastByDate($date);
    }

    public function bulkCreateWeatherForecast(WeatherStation $weatherStation, array $weatherForecastRows)
    {
        return WeatherForecast::insert(array_map(function ($weatherForecastRow) use ($weatherStation) {
            $weatherForecastRow['station_id'] = $weatherStation->id;
            return $weatherForecastRow;
        }, $weatherForecastRows));
    }

    public function getLatestForecast()
    {
        return $this->weatherForecastInterface->getLatestForecast();
    }
}
