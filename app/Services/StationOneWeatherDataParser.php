<?php


namespace App\Services;


use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;

class StationOneWeatherDataParser implements WeatherDataParser
{
    public function parse(string $fileName)
    {
        $fileRows = json_decode(Storage::disk('weather')->get($fileName), true);

        return array_map(function ($fileRow) {

            $fileRow['time'] = Carbon::createFromTimestamp($fileRow['time']);
            $fileRow['temperature'] = (float)($fileRow['temperature'] - 32) * 5 / 9;
            $fileRow['rain'] = (float)$fileRow['rain'] * 25.4;
            $fileRow['wind'] = (float)$fileRow['wind'] * 1.609;
            if ($fileRow['battery_level'] <= 30) {
                $fileRow['battery_level'] = 'low';
            } elseif ($fileRow['battery_level'] <= 60) {
                $fileRow['battery_level'] = 'medium';
            } elseif ($fileRow['battery_level'] <= 99) {
                $fileRow['battery_level'] = 'high';
            } else {
                $fileRow['battery_level'] = 'full';
            }

            return $fileRow;
        }, $fileRows);
    }
}
