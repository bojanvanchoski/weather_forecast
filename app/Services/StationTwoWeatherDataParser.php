<?php


namespace App\Services;

use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;

/**
 * Class StationTwoWeatherDataParser csv files
 * @package App\Services
 */
class StationTwoWeatherDataParser implements WeatherDataParser
{
    public function parse(string $fileName)
    {
        $reader = new \PhpOffice\PhpSpreadsheet\Reader\Csv();
        $filePath = Storage::disk('weather')->path($fileName);
        $spreadsheet = $reader->load($filePath);

        $fileRows = $spreadsheet->getActiveSheet()->toArray();
        array_shift($fileRows);
        return array_map(function ($fileRow) {

            return [
                'time' => Carbon::createFromFormat('d:m:Y, H:i:s', $fileRow[0]),
                'temperature' => $fileRow[1],
                'humidity' => $fileRow[2],
                'wind' => $fileRow[3],
                'rain' => $fileRow[4],
                'light' => $fileRow[5],
                'battery_level' => $fileRow[6],
            ];
            return $fileRow;
        }, $fileRows);
    }
}
