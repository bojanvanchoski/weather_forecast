<?php

namespace App\Providers;

use App\Repositories\WeatherForecastInterface;
use App\Repositories\WeatherForecastRepository;
use App\Repositories\WeatherStationInterface;
use App\Repositories\WeatherStationRepository;
use App\WeatherForecast;
use Illuminate\Support\ServiceProvider;

class RepositoriesServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(WeatherStationInterface::class, WeatherStationRepository::class);
        $this->app->bind(WeatherForecastInterface::class, WeatherForecastRepository::class);
    }
}
