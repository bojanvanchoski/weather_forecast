<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class WeatherStationForecastRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'date' => 'required|date|date_format:"Y-m-d\TH:i:s\.v\Z"'
        ];
    }

    public function messages()
    {
        return [
            'date.required' => 'Date is required!',
            'date.date' => 'Date must be in this format YYYY-MM-DD H-M-S',
        ];
    }
}
