<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\WeatherStationForecastRequest;
use App\Services\WeatherForecastService;
use App\WeatherStation;
use Carbon\Carbon;

class WeatherStationController extends Controller
{
    protected $weatherForecastService;

    public function __construct(WeatherForecastService $weatherForecastService)
    {
        $this->weatherForecastService = $weatherForecastService;
    }

    /**
     * Store a newly created resource in storage.
     */
    public function forecast(WeatherStation $weatherStation, WeatherStationForecastRequest $request)
    {
        $validatedData = $request->validated();

        $startDate = Carbon::parse($validatedData['date']);

        return $this->weatherForecastService->getWeatherForecastByStation($weatherStation, $startDate);
    }
}
