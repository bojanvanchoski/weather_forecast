<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\ForecastRequest;
use App\Services\WeatherForecastService;

class WeatherForecastController extends Controller
{
    protected $weatherForecastService;

    public function __construct(WeatherForecastService $weatherForecastService)
    {
        $this->weatherForecastService = $weatherForecastService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(ForecastRequest $request)
    {
        $validatedData = $request->validated();

        return $this->weatherForecastService->getAverageWeatherForecastByDate($validatedData['date']);
    }

    public function getLatestForecast()
    {
        return $this->weatherForecastService->getLatestForecast();
    }
}
