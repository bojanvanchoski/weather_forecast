<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WeatherStation extends Model
{
    const STATION_TYPE_ONE = 1;
    const STATION_TYPE_TWO = 2;

    protected $fillable = [
        'name',
        'city',
        'type'
    ];
}
