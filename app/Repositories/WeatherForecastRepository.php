<?php

namespace App\Repositories;

use App\WeatherForecast;
use App\WeatherStation;
use \Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class WeatherForecastRepository implements WeatherForecastInterface
{
    public function getWeatherForecastByStation(WeatherStation $weatherStation, string $startDate, string $endDate): Collection
    {
        return DB::table('weather_forecasts')
            ->join('weather_stations', 'weather_forecasts.station_id', '=', 'weather_stations.id')
            ->select('weather_forecasts.temperature', 'weather_forecasts.humidity', 'weather_forecasts.wind')
            ->where('weather_stations.id', '=', $weatherStation->id)
            ->whereBetween('weather_forecasts.time', [$startDate, $endDate])
            ->get();
    }

    public function getAverageWeatherForecastByDate(string $date)
    {
        return DB::select('select avg(temperature) as temperature, avg(humidity) as humidiy, avg(wind) as wind from weather_forecasts where date(time) = :date group by date(time);', ['date' => $date]);
    }

    public function getLatestForecast()
    {
        return WeatherForecast::orderBy('time', 'desc')->first();
    }
}
