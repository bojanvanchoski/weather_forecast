<?php

namespace App\Repositories;

use App\WeatherStation;

interface WeatherForecastInterface
{
    public function getWeatherForecastByStation(WeatherStation $weatherStation, string $startDate, string $endDate);

    public function getAverageWeatherForecastByDate(string $date);

    public function getLatestForecast();
}
