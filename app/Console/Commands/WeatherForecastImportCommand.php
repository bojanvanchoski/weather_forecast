<?php

namespace App\Console\Commands;

use App\Services\StationOneWeatherDataParser;
use App\Services\StationTwoWeatherDataParser;
use App\Services\WeatherForecastService;
use App\WeatherStation;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class WeatherForecastImportCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'forecast:import';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import weather reports into database';

    protected $stationTwoWeatherDataParser;
    protected $stationOneWeatherDataParser;
    protected $weatherForecastService;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(StationTwoWeatherDataParser $stationTwoWeatherDataParser, StationOneWeatherDataParser $stationOneWeatherDataParser, WeatherForecastService $weatherForecastService)
    {
        parent::__construct();

        $this->stationTwoWeatherDataParser = $stationTwoWeatherDataParser;
        $this->stationOneWeatherDataParser = $stationOneWeatherDataParser;
        $this->weatherForecastService = $weatherForecastService;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {
            $fileNameJson = Carbon::today()->format('Y-d-m') . '.json';

            if (Storage::disk('weather')->exists($fileNameJson)) {
                $stationOneWeatherData = $this->stationOneWeatherDataParser->parse($fileNameJson);
                $weatherStation = WeatherStation::where(['type' => WeatherStation::STATION_TYPE_ONE])->firstOrFail();
                $this->weatherForecastService->bulkCreateWeatherForecast($weatherStation, $stationOneWeatherData);
                Storage::disk('weather')->move($fileNameJson, 'parsed' . DIRECTORY_SEPARATOR . $fileNameJson);
            }
        } catch (\Exception $exception) {
            Log::channel('forecast_import')->info($exception->getMessage());

        }
        try {
            $fileNameCsv = Carbon::today()->format('d-m-Y') . '.csv';
            if (Storage::disk('weather')->exists($fileNameCsv)) {
                $stationTwoWeatherData = $this->stationTwoWeatherDataParser->parse($fileNameCsv);
                $weatherStation = WeatherStation::where(['type' => WeatherStation::STATION_TYPE_TWO])->firstOrFail();
                $this->weatherForecastService->bulkCreateWeatherForecast($weatherStation, $stationTwoWeatherData);
                Storage::disk('weather')->move($fileNameCsv, 'parsed' . DIRECTORY_SEPARATOR . $fileNameCsv);
            }
        } catch (\Exception $exception) {
            Log::channel('forecast_import')->info($exception->getMessage());
        }

        return 0;
    }
}
