<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'v1', 'middleware'=>'client'], function () {
    Route::get('weather-forecast/latest','API\WeatherForecastController@getLatestForecast');

    Route::apiResources([
        'weather-station' => 'API\WeatherStationController',
        'weather-forecast' => 'API\WeatherForecastController'
    ]);
    Route::get('weather-station/{weather_station}/forecast','API\WeatherStationController@forecast');
});
