<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWeatherForecastTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('weather_forecasts', function (Blueprint $table) {
            $table->id();
            $table->dateTime('time');
            $table->float('temperature');
            $table->float('humidity');
            $table->float('rain');
            $table->float('wind');
            $table->enum('battery_level',['low','medium','high','full']);
            $table->float('light')->nullable();
            $table->unsignedBigInteger('station_id');

            $table->foreign('station_id')->references('id')->on('weather_stations');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('weather_forecasts');
    }
}
