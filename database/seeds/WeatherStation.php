<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class WeatherStation extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('weather_stations')->insert([
            'name' => 'Station One',
            'city' => 'Skopje',
            'type' => \App\WeatherStation::STATION_TYPE_ONE,
        ]);

        DB::table('weather_stations')->insert([
            'name' => 'Station Two',
            'city' => 'Skopje',
            'type' => \App\WeatherStation::STATION_TYPE_TWO,
        ]);
    }
}
