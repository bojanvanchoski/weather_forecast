This application requires PHP version of minimum 7.1.3 and composer min version 1.10.13 .

After the clone / download the application run:

`composer install`

Create database and cofigure database conection in .env file. Copy .env.example into .env file and change databse configuration into the file.

Generate app key with this command:

`php artisan key:generate` 

After this run:

`php artisan migrate:fresh --seed` 

This will run the migration and seeders for Weather Stations table

After first command, run:

`php artisan passport:client --client`

This command will create Client credentials that should be use for api authentication. After running the command, on the console output will promt the client ID and Client Secret.Examples for using the api routes : 

For importing the weather files from data directory run the followin command: 
Please check first that the file names are same as the date when you are running the command

`php artisan forecast:import`

Run this command to start the server
`php artisan serve`

After this we can use the api endpoint, in the root of the project there is postamn collection file `weather.postman_collection.json` that you can use to test the api.


